# -*- coding: utf-8 -*-
%matplotlib inline
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from sklearn.linear_model import LinearRegression, Lasso, Ridge, ElasticNet
from sklearn.preprocessing import LabelEncoder
from google.cloud import bigquery
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor
from sklearn.neighbors import KNeighborsRegressor

import xgboost as xgb
from xgboost import XGBRegressor
from lightgbm import LGBMRegressor

#一時的にwarnings非表示に
import warnings
warnings.filterwarnings('ignore')

#マイライブラリー読込
from mylib import *

# データの読み込み
train =pd.read_csv('gs://sample_machine_learning_input/HousePrices/train.csv')
test = pd.read_csv('gs://sample_machine_learning_input/HousePrices/test.csv')

#objectの列数値に変換
for i in range(train.shape[1]):
    if train.iloc[:,i].dtypes == object:
        lbl = LabelEncoder()
        lbl.fit(list(train.iloc[:,i].values) + list(test.iloc[:,i].values))
        train.iloc[:,i] = lbl.transform(list(train.iloc[:,i].values))
        test.iloc[:,i] = lbl.transform(list(test.iloc[:,i].values))
        
train["SaleCondition"].unique()

#外れデータ除外
train = train.drop(train[(train['LotArea']>100000)].index)
train = train.drop(train[(train['Street']<0.1)].index)
train = train.drop(train[(train['Utilities']>0.9)].index)
train = train.drop(train[(train['SalePrice']>700000)].index)
train = train.drop(train[(train['BsmtFinSF1']>5000)].index)
train = train.drop(train[(train['Electrical']>4.5)].index)
train = train.drop(train[(train['LowQualFinSF']>560)].index)
train = train.drop(train[(train['GrLivArea']>4500)].index)
train = train.drop(train[(train['BsmtFullBath']>2.5)].index)
train = train.drop(train[(train['BsmtHalfBath']>1.75)].index)
train = train.drop(train[(train['BedroomAbvGr']>7)].index)
train = train.drop(train[(train['KitchenAbvGr']>2.75)].index)
train = train.drop(train[(train['OpenPorchSF']>500)].index)
train = train.drop(train[(train['EnclosedPorch']>500)].index)
train = train.drop(train[(train['SaleCondition']>-1) & (train['SalePrice']>700000)].index)

# データ分割
train_ID = train['Id']
test_ID = test['Id']

y_train = train['SalePrice']
X_train = train.drop(['Id','SalePrice'], axis=1)
X_test = test.drop('Id', axis=1)

#欠落データのチェック
pd.set_option('display.max_rows', 500)
#print(missingData_check(X_train,pd,"学習用データ"))
#print(missingData_check(X_test,pd,"予測用データ"))

#トレーニングデータとテストデータを結合
Xmat = pd.concat([X_train, X_test])

# 欠損が多いカラムを削除
Xmat = Xmat.drop(['LotFrontage','MasVnrArea','GarageYrBlt'], axis=1)

#販売価格のばらつきが顕著なものは除外
Xmat = Xmat.drop(['LotShape','LotConfig','BsmtFinSF1','3SsnPorch','ScreenPorch','PoolArea','PoolQC','MoSold','YrSold'], axis=1)
#Xmat = Xmat.drop(['MSSubClass','MSZoning','Street','LandContour','Utilities','LandSlope','Neighborhood','Condition1','BldgType','HouseStyle','RoofMatl','Exterior1st','Exterior2nd','MasVnrType','ExterCond','Foundation','BsmtQual','BsmtCond','BsmtExposure','BsmtFinType1','BsmtFinType2','Heating','CentralAir','Electrical','LowQualFinSF','BsmtFullBath','BsmtHalfBath','FullBath','HalfBath','BedroomAbvGr','KitchenAbvGr','KitchenQual','Functional','Fireplaces','FireplaceQu','GarageType','GarageQual','GarageCond','PavedDrive','Fence','MiscFeature','MiscVal','SaleType','SaleCondition','SaleCondition'], axis=1)


# 欠損値の少ないカラムのNaNは中央値(median)で埋める
Xmat = Xmat.fillna(Xmat.median())

# 対数計算を実施
y_train = np.log(y_train)

# trainデータとtestデータを含んでいるXmatを、再度trainデータとtestデータに分割
X_train = Xmat.iloc[:train.shape[0],:]
X_test = Xmat.iloc[train.shape[0]:,:]


#欠落データのチェック
pd.set_option('display.max_rows', 500)
#print(missingData_check(X_train,pd,"学習用データ"))
#print(missingData_check(X_test,pd,"予測用データ"))



# 線形回帰(Linear Regression)########
slr = LinearRegression()
slr_data = slr.fit(X_train,y_train)
###End LinearRegression#############

# Lasso Regression ########
clf = Lasso(alpha=0.001)
clf_data = clf.fit(X_train,y_train)
###End Lasso Regression

# GradientBoostingRegressor########
sgr = GradientBoostingRegressor(max_depth=4, n_estimators=150)
sgr_data = sgr.fit(X_train,y_train)
#End GradientBoostingRegressor#######

# Ridge#############################
srd = Ridge(alpha=13)
srd_data = srd.fit(X_train,y_train)
#End Ridge############################


#xgboost################
xgboost = XGBRegressor(learning_rate=0.01, n_estimators=3460,
                                     max_depth=3, min_child_weight=0,
                                     gamma=0, subsample=0.7,
                                     colsample_bytree=0.7,
                                     objective='reg:linear', nthread=-1,
                                     scale_pos_weight=1, seed=27,
                                     reg_alpha=0.00006, random_state=42)
xgboost_data = xgboost.fit(X_train,y_train)

'''
# KNeighborsRegressor########
#knnR = KNeighborsRegressor(n_neighbors=20, n_jobs=-1)
#knnR_data = knnR.fit(X_train,y_train)

#xgboost################
xgb_model = xgb.XGBRegressor()
xgb_model_data=xgb_model.fit(X_train, y_train)

#End xgboost##############################
lightgbm = LGBMRegressor(objective='regression', 
                                       num_leaves=4,
                                       learning_rate=0.01, 
                                       n_estimators=5000,
                                       max_bin=200, 
                                       bagging_fraction=0.75,
                                       bagging_freq=5, 
                                       bagging_seed=7,
                                       feature_fraction=0.2,
                                       feature_fraction_seed=7,
                                       verbose=-1,
                                       #min_data_in_leaf=2,
                                       #min_sum_hessian_in_leaf=11
                                       )
lgb_data = lightgbm.fit(X_train,y_train)

'''



blend_models_predict = ((0.4 * slr_data.predict(X_test)) + (0.3 * xgboost_data.predict(X_test))   
                         + (0.1 * clf_data.predict(X_test)) + (0.1 * sgr_data.predict(X_test)) + (0.1 * srd_data.predict(X_test)) )

y_test_pred = np.exp(blend_models_predict)


'''
print('傾き：{0}'.format(slr.coef_[0]))

print('y切片: {0}'.format(slr.intercept_))

print('パラメータ: {0}'.format(slr.get_params()))

print('決定係数score: {0}'.format(slr.score(X_train,y_train)))  
'''



submission = pd.DataFrame({
    "Id": test_ID,
    "SalePrice": y_test_pred
})
submission.to_csv('gs://sample_machine_learning_output/HousePrices/hp_submission3.csv', index=False)
########

print ('0.9824768356506703')#GradientBoostingRegressor 0.13563
print('0.8242257971358715')#LassoRegression alpha=1.0
print('0.8492010559218236')#LassoRegression alpha=0.1
print('0.9216442320983441')#LassoRegression alpha=0
print('0.9194097286064207')#LinearRegression
