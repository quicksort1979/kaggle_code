#共通処理用関数ファイル


#欠損値算出
def missingData_check(df,pd,title): 
        null_val = df.isnull().sum()
        percent = 100 * df.isnull().sum()/len(df)
        kesson_table = pd.concat([null_val, percent], axis=1)
        kesson_table_ren_columns = kesson_table.rename(
            columns = {0 : title + '欠損値', 1 : '%'})
        return kesson_table_ren_columns