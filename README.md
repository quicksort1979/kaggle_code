- my_tree_out.dat
    - 決定木分析　分岐データ
	
- mylib.py
    - 共通関数

- run_houseprice.ipynb
    - kaggle(House Prices)コンペ　線形回帰

- run_houseprice_2.ipynb
    - kaggle(House Prices)コンペ　線形回帰　Ver2
    
- run_houseprice_3.ipynb
    - kaggle(House Prices)コンペ　線形回帰　Ver3

- run_titanic.ipynb
    - kaggle(タイタニック）

- run_titanic_DecisionTree.ipynb
    - kaggle(タイタニック）ロジスティック回帰サンプル（tensorflow)

- run_titanic_loadmodel.ipynb
    - kaggle(タイタニック）モデルをロードして予測


---
## 追加ライブラリ

~~~
pip3 install xgboost
~~~


---
## スコアメモ

Linear Regression 0.12582

Lasso Regression(alpha=1.0) 0.18889

Lasso Regression(alpha=0.00047) 0.12593

GradientBoostingRegressor 0.13553

Ridge 0.12591

ElasticNet 0.12596

Lasso(alpha =0.001, random_state=1) 0.12611

---
## 今後の参考・ツール等

MLflow : https://qiita.com/fam_taro/items/469a025cbf08129f8393


